<?php

$array = [1, 2, 3, 4, 5];
    
if($_SERVER['REQUEST_METHOD'] == 'GET')
{
    $response['error'] = false;
    $response['message'] = "Array has been returend successfully";
    $response['data'] = $array;
    http_response_code (200);
}

else
{
    $response['error'] = true; 
    $response['message'] = "Invalid Request";
    $response['data'] = [];
    http_response_code (403);
}

echo json_encode($response, JSON_UNESCAPED_UNICODE);

?>